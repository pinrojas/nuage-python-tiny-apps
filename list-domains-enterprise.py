# -*- coding: utf-8 -*-
"""
A simple script that will print out a tree structure for each enterprise the user has access to.
--- Usage ---
python list_enterprises_domains_vms_structure_acls.py
--- Author ---
Philippe Dellaert <philippe.dellaert@nuagenetworks.net>
"""
from vspk.vsdk import v3_2 as vsdk
import time

session = vsdk.NUVSDSession(
        username='csproot',
        password='csproot',
        enterprise='csp',
        api_url='https://10.0.0.2:8443'
)

session.start()
user = session.user
lic=user.licenses.get()
print ('\nLicense expiration date: %s' % time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(lic[0].expiration_date/1000)))

for cur_ent in user.enterprises.get():
    print('\nDomains inside Enterprise %s' % cur_ent.name)
    for cur_domain in cur_ent.domains.get():
        print('|- Domain: %s' % cur_domain.name)
        for cur_zone in cur_domain.zones.get():
            print('    |- Zone: %s' % cur_zone.name)
            for cur_subnet in cur_zone.subnets.get():
                print('        |- Subnets: %s - %s - %s' % (cur_subnet.name, cur_subnet.address, cur_subnet.netmask))
                for cur_vm in cur_subnet.vms.get():
                    print ('            |- Instance: %s' % (cur_vm.name))


    print('--------------------------------------------------------------------------------')




