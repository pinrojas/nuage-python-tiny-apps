#!/usr/bin/env python -u
'''
  
This script does the following
  
1. Create glance image from  http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1508.qcow2
2. Add key pair
3. Create networks, subnets and instances
4. Attach floating IPs
  
mau@nuagenetworks.net

'''

from __future__ import print_function

import datetime
import os.path
import socket
import sys
import subprocess
import time

VERSION = '2'
USERNAME = 'admin'
PASSWORD = 'sag81-sled'
PROJECT_NAME = 'admin'
AUTH_URL = 'http://10.0.0.10:35357/v2.0'
AUTH_URL_N = 'http://10.0.0.10:5000/v2.0'
NET_NAME = 'adm.priv'

from neutronclient.v2_0 import client as neutronclient
neutron = neutronclient.Client(username=USERNAME, password=PASSWORD, tenant_name=PROJECT_NAME, auth_url=AUTH_URL_N)
from novaclient import client as novaclient
nova = novaclient.Client(VERSION, USERNAME, PASSWORD, PROJECT_NAME, AUTH_URL)

#creating key-pair
if not nova.keypairs.findall(name="mykey"):
    print("Creating keypair: mykey...")
    with open(os.path.expanduser('~/.ssh/id_rsa.pub')) as fpubkey:
        nova.keypairs.create(name="mykey", public_key=fpubkey.read())
print("mykey done")

# creating centos-7-x86_64 image (glance)
images = nova.images.list() 
skip = 0
for image in images:
    if image.name == 'centos7-x86_64-01':
         skip = 1
if not skip:
     bash_glance = "glance image-create --name centos7-x86_64-01 --disk-format qcow2 --container-format bare --is-public True --location http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1508.qcow2"
     process = subprocess.Popen(bash_glance.split(), stdout=subprocess.PIPE)
     output = process.communicate()[0]


#get router id
router_list = neutron.list_routers()
router_id = router_list['routers'][0]['id']

i = 0
while i < 4:
    i = i + 1
    net_find = neutron.list_networks(name='adm.priv'+str(i)) 
    if net_find['networks']:
          continue
    body_sample = {'network': {'name': NET_NAME+str(i),
                   'admin_state_up': True}}

    netw = neutron.create_network(body=body_sample)
    net_dict = netw['network']
    network_id = net_dict['id']
    print('Network %s created' % network_id)

    body_create_subnet = {'subnets': [{'cidr': '192.168.5'+str(i)+'.0/24',
                          'ip_version': 4, 'name': NET_NAME+str(i), 'network_id': network_id}]}

    subnet = neutron.create_subnet(body=body_create_subnet)
    sb_dict = subnet['subnets']
    sb_prt = sb_dict[0]
    print('Sub-Network %s created' % sb_prt['id'])

    response = neutron.add_interface_router(router_id,{'subnet_id': sb_prt['id']})
    print('Port %s created' % response)

#loop-ends
go_next = 0
#create instances
nets = neutron.list_networks()
for p in nets['networks']:
	if p['name'][0:8] == 'adm.priv':
                server = nova.servers.list()
                for instance in server:
                     if instance.name == p['name']+'.inst_fip':
                           go_next=1
                if go_next:
                      go_next=0 
                      continue
                body_value = {
                                 "port": {
                                         "admin_state_up": True,
                                         "name": p['name']+'port',
                                         'network_id': p['id']
                                  }
                             }
                port1 = neutron.create_port(body=body_value)
                print('Port %s created' % port1['port']['id'])

                print("Booting instance...", end='')
                image = nova.images.find(name="centos7-x86_64-01")
                flavor = nova.flavors.find(name="m1.small")
                nics = [{'port-id': port1['port']['id']}]
                instance = nova.servers.create(name=p['name']+'.inst_fip', image=image, flavor=flavor, key_name="mykey", nics=nics)
                
                status = instance.status
                while status == 'BUILD':
                         time.sleep(5)
                         # Retrieve the instance again so the status field updates
                         instance = nova.servers.get(instance.id)
                         status = instance.status
                
                print("Creating floating ip...", end='')
                
                # Get external network
                ext_net, = [x for x in neutron.list_networks()['networks']
                    if x['router:external']]
  
                # Get the port corresponding to the instance
                port, = [x for x in neutron.list_ports()['ports']
                    if x['device_id'] == instance.id]
  
                # Create the floating ip
                args = dict(floating_network_id=ext_net['id'],port_id=port['id'])
                ip_obj = neutron.create_floatingip(body={'floatingip': args})
print("done")
